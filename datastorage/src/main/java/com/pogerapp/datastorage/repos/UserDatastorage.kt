package com.pogerapp.datastorage.repos

import com.pogerapp.core.entity.UserModel

interface UserDatastorage {
    fun getUsers(): List<UserModel>
    fun putUsers(users: ArrayList<UserModel>)
    fun clear()
}