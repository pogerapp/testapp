package com.pogerapp.datastorage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pogerapp.datastorage.model.Specialty
import com.pogerapp.datastorage.model.User
import com.pogerapp.datastorage.model.UserSpecialtyCrossReference
import com.pogerapp.datastorage.dao.SpecialtyDao
import com.pogerapp.datastorage.dao.UserDao
import com.pogerapp.datastorage.dao.UserWithSpecDao

@Database(entities = [User::class, Specialty::class, UserSpecialtyCrossReference::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun specialtyDao(): SpecialtyDao
    abstract fun crossRefDao():UserWithSpecDao
}