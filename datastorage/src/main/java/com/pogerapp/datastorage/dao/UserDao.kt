package com.pogerapp.datastorage.dao

import androidx.room.*
import com.pogerapp.datastorage.model.User
import com.pogerapp.datastorage.model.UserWithSpecialities

@Dao
interface UserDao {
    @Transaction
    @Query("SELECT * FROM User")
    fun getUsers(): List<UserWithSpecialities>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: ArrayList<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(users: User)

    @Query("DELETE FROM User")
    fun clear()
}
