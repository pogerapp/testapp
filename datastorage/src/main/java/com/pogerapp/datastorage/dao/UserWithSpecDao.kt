package com.pogerapp.datastorage.dao

import androidx.room.*
import com.pogerapp.datastorage.model.UserSpecialtyCrossReference

@Dao
interface UserWithSpecDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(ref: UserSpecialtyCrossReference)

    @Query("DELETE FROM UserSpecialtyCrossReference")
    fun clear()
}