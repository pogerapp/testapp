package com.pogerapp.datastorage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pogerapp.datastorage.model.Specialty

@Dao
interface SpecialtyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSpecialty(specialty: Specialty)

    @Query("DELETE FROM specialties")
    fun clear()
}