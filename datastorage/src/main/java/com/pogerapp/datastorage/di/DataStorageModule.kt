package com.pogerapp.datastorage.di

import android.content.Context
import com.pogerapp.datastorage.DataStorageImplementation
import com.pogerapp.datastorage.repos.UserDatastorage
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class DataStorageModule {
    @Singleton
    @Provides
    fun provideDataStorage(@ApplicationContext context: Context) =
        DataStorageImplementation(context)
}

@Module
@InstallIn(ApplicationComponent::class)
abstract class DataStorageBindings {
    @Binds
    @Singleton
    abstract fun dataStorageToUserRepo(storage: DataStorageImplementation): UserDatastorage
}