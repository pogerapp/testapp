package com.pogerapp.datastorage

import android.content.Context
import androidx.room.Room
import com.pogerapp.core.entity.UserModel
import com.pogerapp.datastorage.model.Specialty
import com.pogerapp.datastorage.model.User
import com.pogerapp.datastorage.model.UserSpecialtyCrossReference
import com.pogerapp.datastorage.repos.UserDatastorage
import javax.inject.Inject

class DataStorageImplementation @Inject constructor(
    private val context: Context
) : UserDatastorage {

    private val room = Room.databaseBuilder(
        context,
        Database::class.java, "testDB"
    ).build()

    override fun clear() {
        room.userDao().clear()
        room.crossRefDao().clear()
        room.specialtyDao().clear()
    }

    override fun getUsers(): List<UserModel> {
        val result = room.userDao().getUsers()
        return result.map { it.toModel() }
    }

    override fun putUsers(users: ArrayList<UserModel>) {
        clear()
        users.forEach { user ->
            val tempUser = User.fromModel(user)
            room.userDao().insertUser(tempUser)
            user.specialties.forEach { specialty ->
                if (specialty.id != null) {
                    room.specialtyDao()
                        .insertSpecialty(Specialty(specialty.id ?: -1, specialty.name))
                    room.crossRefDao().insert(
                        UserSpecialtyCrossReference(
                            userId = tempUser.userId,
                            specialtyId = specialty.id!!
                        )
                    )
                }
            }
        }
    }
}