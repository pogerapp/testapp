package com.pogerapp.datastorage.model

import androidx.room.*
import com.pogerapp.core.entity.UserModel
import java.util.*

@Entity
data class User(
    @PrimaryKey
    var userId: String = UUID.randomUUID().toString(),

    val firstName: String = "",

    val lastName: String = "",

    val birthday: String? = null,

    val avatar: String? = null,
) {

    companion object {
        fun fromModel(model: UserModel) = User(
            firstName = model.firstName,
            lastName = model.lastName,
            birthday = model.birthday,
            avatar = model.avatar
        )
    }
}