package com.pogerapp.datastorage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "specialties")
data class Specialty(
    @PrimaryKey
    val specialtyId: Int,

    @ColumnInfo(name = "specialtyName")
    val name: String = ""
)
