package com.pogerapp.datastorage.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.pogerapp.core.entity.UserModel

data class UserWithSpecialities(
    @Embedded val user: User,
    @Relation(
        parentColumn = "userId",
        entityColumn = "specialtyId",
        associateBy = Junction(UserSpecialtyCrossReference::class)
    )
    val specialties: List<Specialty>
){
        fun toModel() = UserModel(
            firstName = user.firstName,
            lastName = user.lastName,
            birthday = user.birthday,
            avatar = user.avatar,
            specialties = ArrayList<com.pogerapp.core.entity.Specialty>().apply{
                addAll(specialties.map { com.pogerapp.core.entity.Specialty(it.specialtyId, it.name) })
            }
        )

}