package com.pogerapp.datastorage.model

import androidx.room.Entity

@Entity(primaryKeys = ["userId", "specialtyId"])
data class UserSpecialtyCrossReference(
    val userId: String,
    val specialtyId: Int
)
