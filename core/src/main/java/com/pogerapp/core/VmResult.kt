package com.pogerapp.core

sealed class VmResult<T>(val success: T? = null, val error: Throwable? = null) {
    class Loading<T> : VmResult<T>(null, null)
    class Success<T>(success: T) : VmResult<T>(success, null)
    class Error<T>(error: Throwable) : VmResult<T>(null, error)
}