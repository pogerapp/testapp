package com.pogerapp.core

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController

abstract class NavFragment(layoutId: Int) : BaseFragment(layoutId) {
    abstract val viewModel: AndroidViewModel
    abstract fun navController(): NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(viewModel is NavCompViewModel) {
            (viewModel as NavCompViewModel).navController = navController()
        }
    }
}