package com.pogerapp.core

import androidx.fragment.app.Fragment

open class BaseFragment(layoutId: Int): Fragment(layoutId)