package com.pogerapp.core

import android.content.res.Resources
import android.util.TypedValue
import androidx.core.content.res.ResourcesCompat

val Int.dp
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        Resources.getSystem().displayMetrics
    ).toInt()