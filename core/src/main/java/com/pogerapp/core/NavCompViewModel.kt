package com.pogerapp.core

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController

open class NavCompViewModel(app: Application): AndroidViewModel(app) {
    var navController: NavController? = null
}