package com.pogerapp.core

data class CoreResult<T>(val success: T? = null, val error: Throwable? = null) {
    var isSuccess = error == null

    fun parse(onSuccess: (T) -> Unit, onError: (Throwable) -> Unit) {
        if (isSuccess) onSuccess(success!!)
        else onError(error!!)
    }
}