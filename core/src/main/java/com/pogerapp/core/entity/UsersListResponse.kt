package com.pogerapp.core.entity

import com.google.gson.annotations.SerializedName

data class UsersListResponse (
    @SerializedName("response")
    val response: ArrayList<UserModel> = ArrayList()
){
    fun convertDates(){

    }
}