package com.pogerapp.core.entity

import com.google.gson.annotations.SerializedName

data class Specialty(
    @SerializedName("specialty_id")
    val id: Int? = null,

    @SerializedName("name")
    val name: String = ""
)