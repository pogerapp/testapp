package com.pogerapp.core.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

data class UserModel(
    @SerializedName("f_name")
    val firstName: String = "",

    @SerializedName("l_name")
    val lastName: String = "",

    @SerializedName("birthday")
    val birthday: String? = null,

    @SerializedName("avatr_url")
    val avatar: String? = null,

    @SerializedName("specialty")
    val specialties: ArrayList<Specialty> = ArrayList()
) : Serializable {

    private val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    fun getBirthdayDate() = try {
        sdf.parse(birthday)
    } catch (ex: Exception) {
        null
    }
}