package com.pogerapp.core

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginItemDecoration(
    val leftDp: Int = 16.dp,
    val topDp: Int = 5.dp,
    val rightDp: Int = 16.dp,
    val bottomDp: Int = 5.dp
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        view.setPadding(leftDp, topDp, rightDp, bottomDp)
    }
}