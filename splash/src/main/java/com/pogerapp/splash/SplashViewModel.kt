package com.pogerapp.splash

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import com.pogerapp.core.NavCompViewModel
import com.pogerapp.test65apps.NavMainDirections

class SplashViewModel @ViewModelInject constructor(
    private val app: Application
) : NavCompViewModel(app) {

    fun goFurther() {
        android.os.Handler().postDelayed({
            navController?.navigate(NavMainDirections.actionGlobalToUsersList())
        }, 2000)
    }
}