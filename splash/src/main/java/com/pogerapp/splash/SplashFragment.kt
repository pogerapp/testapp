package com.pogerapp.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.pogerapp.core.NavFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : NavFragment(R.layout.fragment_splash) {
    override val viewModel by viewModels<SplashViewModel> ()
    override fun navController() = findNavController()

    override fun onResume() {
        super.onResume()
        viewModel.goFurther()
    }
}