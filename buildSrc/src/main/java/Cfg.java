public class Cfg {
    public static final int minSdkVersion = 21;
    public static final int targetSdkVersion = 29;
    public static final int compileSdkVersion = 29;
    public static final String buildToolsVersion = "29.0.3";
}
