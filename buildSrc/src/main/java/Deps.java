public class Deps {
    private static final String SUPPORT_LIB_VERSION = "1.2.0";
    private static final String RETROFIT_VERSION = "2.9.0";
    private static final String KOTLIN_COROUTINES_VERSION = "1.3.9";
    private static final String ROOM_VERSION = "2.2.5";
    static final String KOTLIN_VERSION = "1.4.0";
    static final String NAV_VERSION = "2.3.0";
    static final String GSON_VERSION = "2.8.6";
    static final String FRAGMENT_VERSION = "1.3.0-alpha07";
    static final String GLIDE_VERSION = "4.11.0";
    static final String MATERIAL_LIBRARY_VERSION = "1.2.0";
    static final String HILT_VERSION = "2.28-alpha";

    static final String kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:" + KOTLIN_VERSION;
    static final String kotlinCoroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:" + KOTLIN_COROUTINES_VERSION;
    static final String kotlinCoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:" + KOTLIN_COROUTINES_VERSION;
    static final String kotlinSerialization = "org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0";

    static final String supportAppCompat = "androidx.appcompat:appcompat:" + SUPPORT_LIB_VERSION;
    static final String supportDesign = "com.google.android.material:material:" + SUPPORT_LIB_VERSION;
    static final String cardView = "androidx.cardview:cardview:" + SUPPORT_LIB_VERSION;
    static final String recyclerView = "androidx.recyclerview:recyclerview:1.1.0";
    static final String lifecycleLibrary = "androidx.lifecycle:lifecycle-extensions:2.2.0";
    static final String archLifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:2.2.0";
    static final String materialLibrary = "com.google.android.material:material:" + MATERIAL_LIBRARY_VERSION;

    static final String navComp = "androidx.navigation:navigation-fragment-ktx:" + NAV_VERSION;
    static final String navCompUI = "androidx.navigation:navigation-ui-ktx:" + NAV_VERSION;

    static final String gson = "com.google.code.gson:gson:" + GSON_VERSION;

    static final String retrofit = "com.squareup.retrofit2:retrofit:" + RETROFIT_VERSION;
    static final String retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:" + RETROFIT_VERSION;
    static final String retrofitSerialization = "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:0.5.0";
    static final String retrofitLogging = "com.squareup.okhttp3:logging-interceptor:4.8.1";

    static final String glide = "com.github.bumptech.glide:glide:" + GLIDE_VERSION;
    static final String glideProcessor = "com.github.bumptech.glide:compiler:" + GLIDE_VERSION;
    static final String glideRecyclerViewIntegration = "com.github.bumptech.glide:recyclerview-integration:" + GLIDE_VERSION;
    static final String glideOkHttpIntegration = "com.github.bumptech.glide:okhttp3-integration:" + GLIDE_VERSION;

    static final String timber = "com.jakewharton.timber:timber:4.7.1";

    static final String junit = "junit:junit:4.12";
    static final String espressoCore = "com.android.support.test.espresso:espresso-core:3.0.2";
    static final String runner = "com.android.support.test:runner:1.0.2";

    static final String hilt = "com.google.dagger:hilt-android:" + HILT_VERSION;
    static final String hiltKapt = "com.google.dagger:hilt-android-compiler:" + HILT_VERSION;
    static final String hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha01";
    static final String hiltxKapt = "androidx.hilt:hilt-compiler:1.0.0-alpha01";


    static final String room = "androidx.room:room-runtime:" + ROOM_VERSION;
    static final String roomKapt = "androidx.room:room-compiler:" + ROOM_VERSION;
    static final String roomExt = "androidx.room:room-ktx:" + ROOM_VERSION;

    static final String fragment = "androidx.fragment:fragment:" + FRAGMENT_VERSION;
    static final String fragmentKtx = "androidx.fragment:fragment-ktx:" + FRAGMENT_VERSION;

    static final String circleImageView = "de.hdodenhof:circleimageview:3.1.0";
}
