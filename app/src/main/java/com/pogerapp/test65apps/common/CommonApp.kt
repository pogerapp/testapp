package com.pogerapp.test65apps.common

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CommonApp:Application()