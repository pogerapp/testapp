package com.pogerapp.user_list.usecase

import com.pogerapp.core.CoreResult
import com.pogerapp.datastorage.repos.UserDatastorage
import com.pogerapp.datastorage.repos.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetUsersListUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val userDataStorage: UserDatastorage
){
    suspend operator fun invoke() = withContext(Dispatchers.IO){
        val result = userRepository.getUsersList()
        if(result.isSuccess){
            userDataStorage.putUsers(result.success?.response?:ArrayList())
        }
        CoreResult(userDataStorage.getUsers(), result.error)
    }
}