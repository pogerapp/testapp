package com.pogerapp.user_list

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.pogerapp.core.NavCompViewModel
import com.pogerapp.core.SingleLiveEvent
import com.pogerapp.core.VmResult
import com.pogerapp.core.entity.UserModel
import com.pogerapp.user_list.usecase.GetUsersListUseCase
import kotlinx.coroutines.launch

class UsersListViewModel @ViewModelInject constructor(
    val app: Application,
    private val getUsersListUseCase: GetUsersListUseCase
) : NavCompViewModel(app) {

    val usersLiveData = SingleLiveEvent<VmResult<List<UserModel>>>()

    fun getUsers() {
        viewModelScope.launch {
            usersLiveData.value = VmResult.Loading()
            val result = getUsersListUseCase()
            if (result.isSuccess) {
                usersLiveData.value = VmResult.Success(result.success?:arrayListOf())
            } else {
                if (result.success != null) {
                    usersLiveData.value = VmResult.Success(result.success!!)
                }
                usersLiveData.value = VmResult.Error(result.error!!)
            }
        }
    }

    fun goToDetails(user: UserModel) {
        navController?.navigate(
            UsersListFragmentDirections.actionUsersListFragmentToUserDetailsFragment(
                user
            )
        )
    }
}