package com.pogerapp.user_list.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pogerapp.core.entity.UserModel
import com.pogerapp.user_list.R
import kotlinx.android.synthetic.main.user_item.view.*
import java.util.*
import kotlin.collections.ArrayList

class UsersListAdapter(val onClick: (UserModel) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = ArrayList<UserModel>()

    fun setUsers(users: List<UserModel>) {
        items.clear()
        items.addAll(users)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_item,
                parent,
                false
            )
        ).apply {
            itemView.setOnClickListener {
                if(adapterPosition != RecyclerView.NO_POSITION){
                    onClick(items[adapterPosition])
                }
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UserViewHolder).bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(user: UserModel) {
            itemView.userName.text =
                user.firstName.toLowerCase(Locale.ROOT).capitalize(Locale.ROOT) +
                        " " +
                        user.lastName.toLowerCase(Locale.ROOT).capitalize(Locale.ROOT)

            Glide.with(itemView.userAvatar)
                .load(user.avatar)
                .into(itemView.userAvatar)
        }
    }
}