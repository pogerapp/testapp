package com.pogerapp.user_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.pogerapp.core.MarginItemDecoration
import com.pogerapp.core.NavFragment
import com.pogerapp.core.VmResult
import com.pogerapp.user_list.adapter.UsersListAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_users_list.*

@AndroidEntryPoint
class UsersListFragment : NavFragment(R.layout.fragment_users_list) {
    override val viewModel by viewModels<UsersListViewModel>()
    override fun navController() = findNavController()

    private val adapter = UsersListAdapter {
        viewModel.goToDetails(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(usersList) {
            layoutManager = LinearLayoutManager(context)
            this.adapter = this@UsersListFragment.adapter
            addItemDecoration(MarginItemDecoration())
        }

        swipeRefresh.setOnRefreshListener {
            viewModel.getUsers()
        }

        initObservers()
        viewModel.getUsers()
    }

    private fun initObservers() {
        viewModel.usersLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is VmResult.Loading -> {
                    swipeRefresh.isRefreshing = true
                }
                is VmResult.Success -> {
                    swipeRefresh.isRefreshing = false
                    adapter.setUsers(it.success ?: ArrayList())
                }
                is VmResult.Error -> {
                    swipeRefresh.isRefreshing = false
                    if (view != null) {
                        Snackbar.make(
                            usersListContainer,
                            it.error?.message ?: getString(R.string.smthWentWrong),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }
}