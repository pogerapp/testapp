package com.pogerapp.datastorage

import com.pogerapp.core.CoreResult
import retrofit2.Response

suspend fun <T> handleError(block: suspend () -> Response<T>): CoreResult<T> {
    return try {
        val result = block()
        CoreResult(result.body(), null)
    } catch (ex: Exception) {
        CoreResult(null, ex)
    }
}