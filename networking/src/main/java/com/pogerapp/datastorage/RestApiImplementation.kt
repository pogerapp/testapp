package com.pogerapp.datastorage

import com.pogerapp.datastorage.repos.UserRepository
import javax.inject.Inject

class RestApiImplementation @Inject constructor(
    private val restClient: RestApi
): UserRepository {
    override suspend fun getUsersList() = handleError {
        restClient.getUsersList()
    }
}