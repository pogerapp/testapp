package com.pogerapp.datastorage

import com.pogerapp.core.entity.UsersListResponse
import retrofit2.Response
import retrofit2.http.GET

interface RestApi {
    @GET("65gb/static/raw/master/testTask.json")
    suspend fun getUsersList(): Response<UsersListResponse>
}