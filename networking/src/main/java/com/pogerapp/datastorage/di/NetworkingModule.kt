package com.pogerapp.datastorage.di

import com.pogerapp.datastorage.BuildConfig
import com.pogerapp.datastorage.RestApi
import com.pogerapp.datastorage.RestApiImplementation
import com.pogerapp.datastorage.repos.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val NETWORK_TIMEOUT_SEC = 30L

@Module
@InstallIn(ApplicationComponent::class)
class NetworkingModule {
    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient) = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .baseUrl(BuildConfig.BASE_URL)
        .build()
        .create(RestApi::class.java)

    @Singleton
    @Provides
    fun provideOkHtpClient(loggingInterceptor: Interceptor) = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .retryOnConnectionFailure(true)
        .connectTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
        .readTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
        .writeTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
        .build()

    @Singleton
    @Provides
    fun provideLoggingInterceptor() : Interceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Singleton
    @Provides
    fun provideRestClient(retrofit: RestApi) = RestApiImplementation(retrofit)
}

@Module
@InstallIn(ApplicationComponent::class)
abstract class NetworkBindings{
    @Singleton
    @Binds
    abstract fun bindRestClientToUsersRepo(restApiImplementation: RestApiImplementation): UserRepository
}