package com.pogerapp.datastorage.repos

import com.pogerapp.core.CoreResult
import com.pogerapp.core.entity.UsersListResponse

interface UserRepository {
    suspend fun getUsersList(): CoreResult<UsersListResponse>
}