package com.pogerapp.user_details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.pogerapp.core.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_user_details.*
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class UserDetailsFragment : BaseFragment(R.layout.fragment_user_details) {

    private val sdf = SimpleDateFormat("dd.MM.yyyy г.", Locale.getDefault())

    private val args by navArgs<UserDetailsFragmentArgs>()
    private val user by lazy { args.user }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this).load(user.avatar).into(avatar)
        userName.text = user.firstName.toLowerCase(Locale.ROOT).capitalize(Locale.ROOT) +
                    " " +
                    user.lastName.toLowerCase(Locale.ROOT).capitalize(Locale.ROOT)

        title.text = userName.text

        with(user.getBirthdayDate()){
            dateTV.text = if (this == null){
                "-"
            }else{
                sdf.format(user.getBirthdayDate()!!)
            }
        }

        backBtn.setOnClickListener {
            findNavController().navigateUp()
        }

        specialties.removeAllViews()
        user.specialties.forEach {
            specialties.addView(Chip(context).apply {
                isClickable = false
                isCheckable = false
                text = it.name
            })
        }
    }
}